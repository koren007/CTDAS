# CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017,2018,2019 Wouter Peters. 
# Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
# updates of the code. See also: http://www.carbontracker.eu. 
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation, 
# version 3. This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 
#
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <http://www.gnu.org/licenses/>. 

# CTDAS code can be downloaded from https://git.wur.nl/woude033/test_CTDAS.git

#!/bin/bash
set -e

usage="$basename "$0" [arg1] [arg2]  [-h] -- script to start a new CTDAS run  

where:
     arg1: directory to run from (i.e., /scratch/"$USER"/)
     arg2: project name (i.e, test_ctdas)
     -h  shows this help text

     ! A new folder will then be created and populated:

     /scratch/"$USER"/test_ctdas/

    "

while getopts ':hs:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done

EXPECTED_ARGS=2

if [[ $# -ne $EXPECTED_ARGS ]]; then
    printf "Missing arguments to function, need $EXPECTED_ARGS \n\n"
    echo "$usage"
    exit 2
fi


echo "New project to be started in folder $1"
echo "               ...........with name $2"

rootdir=$1/$2
rundir=$1/$2/exec
sedrundir=$1/$2/exec

if [ -d "$rootdir" ]; then
    echo "Directory already exists, please remove before running $0"
    exit 1
fi

mkdir -p ${rundir}
rsync -au --cvs-exclude * ${rundir}/
cd ${rundir}

echo "Creating jb file, py file, and rc-file"
sed -e "s/template/$2/g" templates/template.jb > $2.jb
sed -e "s/template/$2/g" templates/template.py > $2.py
sed -e "s,template,${rootdir},g" templates/template.rc > $2.rc
rm -f start_ctdas.sh

chmod u+x $2.jb

echo ""
echo "************* NOW USE ****************"
ls -lrta $2.*
echo "**************************************"
echo ""
cd ${rundir}
pwd


